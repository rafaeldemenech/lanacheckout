FROM golang:alpine AS gobuilder
RUN mkdir -p /go/src/lanacheckout
COPY . /go/src/lanacheckout
RUN apk update && apk add git && go get -u github.com/golang/dep/cmd/dep
WORKDIR /go/src/lanacheckout
RUN dep ensure 
RUN go build

#build frontend project
FROM node as nodebuilder
RUN mkdir -p /go/src/lanacheckout
COPY . /go/src/lanacheckout
WORKDIR /go/src/lanacheckout/appweb
RUN npm install
RUN npm run build

#final stage
FROM alpine:latest
RUN apk --no-cache add ca-certificates
RUN mkdir /app
COPY --from=gobuilder /go/src/lanacheckout/lanacheckout /app
COPY --from=gobuilder /go/src/lanacheckout/config.env /app 
COPY --from=nodebuilder /go/src/lanacheckout/appweb/dist /app/appweb/dist
#ENTRYPOINT ./app/lanacheckout
LABEL Name=lanacheckout Version=0.0.1
EXPOSE 8000
CMD ["/app/lanacheckout"]

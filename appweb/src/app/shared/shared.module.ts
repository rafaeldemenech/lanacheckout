import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

@NgModule({
  declarations: [],
  imports: [CommonModule, LoadingBarHttpClientModule],
  exports: [HttpClientModule, FormsModule, ReactiveFormsModule, LoadingBarHttpClientModule]
})
export class SharedModule {}

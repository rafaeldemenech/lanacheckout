import { Component, OnInit } from "@angular/core";
import { ProductModel, ProductsResponse } from "../model/product.model";
import { MainService } from "../services/main.service";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"]
})
export class ProductComponent implements OnInit {
  busy: boolean;
  productSource: ProductModel[];

  constructor(private mainService: MainService) {}

  ngOnInit() {
    this.loadProducts();
  }

  loadProducts(): void {
    this.busy = true;
    this.mainService
      .GetProducts()
      .subscribe((response: ProductsResponse) => {
        this.productSource = response.data;
        this.busy = false;
      });
  }
}

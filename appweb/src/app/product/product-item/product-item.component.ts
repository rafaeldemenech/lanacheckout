import { Component, OnInit, Input } from '@angular/core';
import { ProductModel } from 'src/app/model/product.model';
import { MainService } from 'src/app/services/main.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ShoppingCartActionResponse } from 'src/app/model/shoppingcart.model';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {
  @Input() entity: ProductModel;
  @Input() index: number;

  mainForm: FormGroup;
  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.initMainForm();
  }

  initMainForm() {

    this.mainForm = new FormGroup({
      'qty': new FormControl(1, [Validators.required, Validators.min(1)])
    });
  }

  onSubmit() {
    const frmModel = this.mainForm.value;

    this.mainService.AddItem(this.entity.code, frmModel.qty).subscribe((result:ShoppingCartActionResponse) =>{
      console.log(result);
      this.mainForm.reset();
    })
  }

}

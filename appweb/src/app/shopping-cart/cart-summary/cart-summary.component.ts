import { Component, OnInit, OnDestroy } from "@angular/core";
import {
  ShoppingCartSummaryModel,
  ShoppingCartSummaryResponse,
  ShoppingCartActionResponse
} from "src/app/model/shoppingcart.model";
import { Subscription } from "rxjs";
import { MainService } from "src/app/services/main.service";

@Component({
  selector: "app-cart-summary",
  templateUrl: "./cart-summary.component.html",
  styleUrls: ["./cart-summary.component.scss"]
})
export class CartSummaryComponent implements OnInit, OnDestroy {
  busy: boolean;
  entity: ShoppingCartSummaryModel;
  subscriptionShoppingItems: Subscription;

  constructor(private mainService: MainService) {}

  ngOnInit() {
    this.resetSummaryModel();
    this.loadShoppingCartItems();
    this.subscriptionShoppingItems = this.mainService.ShoppingCartItemsWatcher.subscribe(
      listChanged => {
        this.loadShoppingCartItems();
      }
    );
  }

  resetSummaryModel(): void {
    this.entity = {} as ShoppingCartSummaryModel;
    this.entity.total_items = 0;
    this.entity.total_value = 0;
    this.entity.deals_applied = [];
  }

  loadShoppingCartItems(): void {
    this.busy = true;
    this.mainService
      .GetBasketSummary()
      .subscribe((response: ShoppingCartSummaryResponse) => {
        if (response.data) this.entity = response.data;
        else this.resetSummaryModel();
        this.busy = false;
      });
  }

  onRemoveBasket(): void {
    this.busy = true;
    this.mainService
      .CleanBasket()
      .subscribe((response: ShoppingCartActionResponse) => {
        this.busy = false;
      });
  }

  ngOnDestroy(): void {
    this.subscriptionShoppingItems.unsubscribe();
  }
}

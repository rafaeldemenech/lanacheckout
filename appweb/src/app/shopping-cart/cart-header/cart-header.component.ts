import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShoppingCartSummaryModel, ShoppingCartSummaryResponse } from 'src/app/model/shoppingcart.model';
import { Subscription } from 'rxjs';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-cart-header',
  templateUrl: './cart-header.component.html',
  styleUrls: ['./cart-header.component.scss']
})
export class CartHeaderComponent implements OnInit, OnDestroy {
  busy: boolean;
  entity: ShoppingCartSummaryModel;
  subscriptionShoppingItems: Subscription;

  constructor(private mainService: MainService) {}

  ngOnInit() {
    this.resetSummaryModel();
    this.loadShoppingCartItems();
    this.subscriptionShoppingItems = this.mainService.ShoppingCartItemsWatcher.subscribe(
      listChanged => {
        this.loadShoppingCartItems();
      }
    );
  }

  resetSummaryModel(): void {
    this.entity = {} as ShoppingCartSummaryModel;
    this.entity.total_items = 0;
    this.entity.total_value = 0;
    this.entity.deals_applied = [];
  }

  loadShoppingCartItems(): void {
    this.busy = true;
    this.mainService
      .GetBasketSummary()
      .subscribe((response: ShoppingCartSummaryResponse) => {
        if (response.data) this.entity = response.data;
        else this.resetSummaryModel();
        this.busy = false;
      });
  }


  ngOnDestroy(): void {
    this.subscriptionShoppingItems.unsubscribe();
  }
}

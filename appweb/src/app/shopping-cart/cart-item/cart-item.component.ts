import { Component, OnInit, Input } from '@angular/core';
import { ShoppingCartItemModel, ShoppingCartActionResponse } from 'src/app/model/shoppingcart.model';
import { MainService } from 'src/app/services/main.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {
  @Input() entity: ShoppingCartItemModel;
  @Input() index: number;

  constructor(private mainService: MainService) { }

  ngOnInit() {
  }

  onSubmit() {
 
    this.mainService.RemoveItem(this.entity.code, this.entity.quantity).subscribe((result: ShoppingCartActionResponse) =>{
      console.log(result);
    })
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShoppingCartItemModel, ShoppingCartResponse } from '../model/shoppingcart.model';
import { MainService } from '../services/main.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  
  busy: boolean;
  shoppingCartSource: ShoppingCartItemModel[];
  subscriptionShoppingItems: Subscription;

  constructor(private mainService: MainService) {}

  ngOnInit() {
    this.loadShoppingCartItems();
    this.subscriptionShoppingItems = this.mainService.ShoppingCartItemsWatcher.subscribe(listChanged => {
      this.loadShoppingCartItems();
    })
  }

  loadShoppingCartItems(): void {
    this.busy = true;
    this.mainService
      .GetBasketItems()
      .subscribe((response: ShoppingCartResponse) => {
        this.shoppingCartSource = response.data;
        this.busy = false;
      });
  }

  ngOnDestroy(): void {
    this.subscriptionShoppingItems.unsubscribe();
  }
}

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Subject, Observable, throwError } from 'rxjs';
import { retry, catchError, tap } from 'rxjs/operators';
import { ProductsResponse } from '../model/product.model';
import { environment } from 'src/environments/environment';
import { ShoppingCartActionResponse, ShoppingCartResponse, ShoppingCartSummaryResponse } from '../model/shoppingcart.model';

const httpOptions = {withCredentials: true};

@Injectable({
  providedIn: 'root'
})
export class MainService {
  private apiURL = environment.apiUrl;
  public ShoppingCartItemsWatcher = new Subject<boolean>();

  constructor(private http: HttpClient) { }

  GetProducts(): Observable<ProductsResponse> {
    return this.http.get<ProductsResponse>(this.apiURL + 'product', httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  AddItem(prodCode: string, qty: number): Observable<ShoppingCartActionResponse> {
    return this.http.post<ShoppingCartActionResponse>(this.apiURL + 'shopping-cart/add/' + prodCode +'/' + qty, null, httpOptions)
      .pipe(
        tap(()=>{
          this.ShoppingCartItemsWatcher.next(true);
        }),
        catchError(this.handleError)
      )
  }

  RemoveItem(prodCode: string, qty: number): Observable<ShoppingCartActionResponse> {
    return this.http.post<ShoppingCartActionResponse>(this.apiURL + 'shopping-cart/remove/' + prodCode +'/' + qty, null, httpOptions)
      .pipe(
        tap(()=>{
          this.ShoppingCartItemsWatcher.next(true);
        }),
        catchError(this.handleError)
      )
  }

  GetBasketItems() : Observable<ShoppingCartResponse>{
    return this.http.get<ShoppingCartResponse>(this.apiURL + 'shopping-cart', httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  GetBasketSummary() : Observable<ShoppingCartSummaryResponse>{
    return this.http.get<ShoppingCartSummaryResponse>(this.apiURL + 'shopping-cart/summary', httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  CleanBasket() : Observable<ShoppingCartActionResponse>{
    return this.http.post<ShoppingCartActionResponse>(this.apiURL + 'shopping-cart/remove-basket', null, httpOptions)
      .pipe(
        tap(()=>{
          this.ShoppingCartItemsWatcher.next(true);
        }),
        catchError(this.handleError)
      )
  }

  private handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}

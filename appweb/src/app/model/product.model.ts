export interface ProductsResponse {
  status: number;
  message: string;
  count: number;
  data: ProductModel[];
}

export interface ProductModel {
  code: string;
  name: string;
  price: number;
  deal_info: string;
}

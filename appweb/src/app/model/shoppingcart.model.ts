export interface ShoppingCartActionResponse {
  status: number;
  message: string;
  affected: number;
}

export interface ShoppingCartResponse {
  status: number;
  message: string;
  count: number;
  data: ShoppingCartItemModel[];
}

export interface ShoppingCartItemModel {
  code: string;
  name: string;
  price: number;
  quantity: number;
}

export interface ShoppingCartSummaryResponse {
  status: number;
  message: string;
  count: number;
  data: ShoppingCartSummaryModel;
}

export interface ShoppingCartSummaryModel {
  total_items: number;
  subtotal_value: number;
  total_discounts: number;
  total_value: number;
  deals_applied: DealModel[];
}

export interface DealModel {
  title: string;
  description: string;
  quantity: number;
  total_value: number;
  total_discount: number;
}

# Lana Go Challenge

> Author: **Rafael Demenech**\
> Email: rafaeldemenech@gmail.com\
> Public Repository: https://gitlab.com/rafaeldemenech/lanacheckout

#### About the project:

##### The project was build in two parts:
- Backend
    - Developed using Golang as a webapi, listen a port to receive requests related to the project;
        - Go Version: go1.13.5 windows/amd64
- Frontend
    - Developed using Angular2+, to provide a good UX and make the challenge more visual;
        - Angular Version: 8.2.14
        - Angular CLI: 8.3.23
        - Node: 12.14.0
###### IDE:
- Visual Studio Code
    - Version: 1.41.1

--------------
##### BUILD AND RUN
###### Using Docker:

-   To build:
``docker build --no-cache -t lanacheckout .``

-   To Run:
``docker run --rm -p 8000:8000 lanacheckout``

###### Using cmd line

1. build the FrontEnd Project to be hosted by the go app:
````
cd appweb
npm install
npm run build
````

2. build the Go app:
In the root folder of the repository
````
go build
````

3. Run the app (it's mandatory to execute the step 1 - build FrontEnd)
    1. to run the app using the app built
        1. On Windows:
        ``./lanacheckout.exe``
        2. On Linux:
        ``./lanacheckout``
    2. to run the app using go
    ``go run main.go``
4. Run the FrontEnd Project (it's possible to run the fronend project alone as a separeted , for developmen or maintence mode)
    1. in this case follow these steps:
    ````
    cd appweb
    npm install
    ng serve
    ````

----

##### WEBAPI
###### It used postman to test the webapi and exported the code to run using ``curl``.

1. Get all active shopping carts in the current execution
``curl --location --request GET http://localhost:8000/api/v1/shopping-cart/all``
2. Get the list of products available
``curl --location --request GET http://localhost:8000/api/v1/product``
3. Get a product by code param
``curl --location --request GET http://localhost:8000/api/v1/product/{prod_code}``
4. Add product to the basket
``curl --location --request POST http://localhost:8000/api/v1/shopping-cart/add/{prod_code}/{qty}``
5. Remove producy from the basket
``curl --location --request POST http://localhost:8000/api/v1/shopping-cart/remove/{prod_code}/{qty}``
6. Clear/Remove the entire basket
``curl --location --request POST http://localhost:8000/api/v1/shopping-cart/remove-basket``
7. Get basket items
``curl --location --request GET http://localhost:8000/api/v1/shopping-cart``
8. Get Basket Summary (Totals + Discounts/Promotions/Deals)
``curl --location --request GET http://localhost:8000/api/v1/shopping-cart/summary``
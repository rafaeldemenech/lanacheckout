package appapi

import (
	"lanacheckout/appapi/controller"
	"lanacheckout/appapi/shared/appconfig"
	"lanacheckout/appapi/shared/sessionstore"
	"lanacheckout/appapi/store"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
)

//Init the webapi
func Init() {
	appconfig.Init()
	sessionstore.Init()
	store.Init()
	startwebapp()
}

func startwebapp() {

	apiVersion := appconfig.EnvKeys.WebAPIVersion

	approuter := mux.NewRouter()

	//Product related routes
	approuter.HandleFunc(apiVersion+"/product", controller.ProductCtrl.Get).Methods(http.MethodGet)
	approuter.HandleFunc(apiVersion+"/product/{code}", controller.ProductCtrl.GetByCode).Methods(http.MethodGet)

	//Shopping cart related routes
	approuter.HandleFunc(apiVersion+"/shopping-cart", controller.ShoppingCartCtrl.Get).Methods(http.MethodGet)
	approuter.HandleFunc(apiVersion+"/shopping-cart/all", controller.ShoppingCartCtrl.ShowAllCarts).Methods(http.MethodGet)
	approuter.HandleFunc(apiVersion+"/shopping-cart/summary", controller.ShoppingCartCtrl.CalcBasketPrice).Methods(http.MethodGet)
	approuter.HandleFunc(apiVersion+"/shopping-cart/add/{code}/{quantity}", controller.ShoppingCartCtrl.AddItem).Methods(http.MethodPost)
	approuter.HandleFunc(apiVersion+"/shopping-cart/remove/{code}/{quantity}", controller.ShoppingCartCtrl.RemoveItem).Methods(http.MethodPost)
	approuter.HandleFunc(apiVersion+"/shopping-cart/remove-basket", controller.ShoppingCartCtrl.RemoveBasket).Methods(http.MethodPost)

	//ngapp - front end
	approuter.PathPrefix("/").Handler(http.FileServer(http.Dir(appconfig.EnvKeys.WebUIPath)))

	//Middleware to set some settings
	approuter.Use(middleware)

	srv := &http.Server{
		Handler:      approuter,
		Addr:         appconfig.EnvKeys.WebPort,
		WriteTimeout: 20 * time.Second,
		ReadTimeout:  20 * time.Second,
	}

	// Run the server in a goroutine.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	log.Println("Starting Lana Service, please open the browser: http://localhost" + appconfig.EnvKeys.WebPort)
	<-c
	log.Println("Shutting down the app...")
}

//middleware customize the requests and log the path called
func middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		originRequest := r.Header.Get("Origin")
		log.Println(originRequest, r.Method, r.RequestURI)

		if len(originRequest) == 0 {
			originRequest = "*"
		}
		w.Header().Set("Access-Control-Allow-Origin", originRequest)
		w.Header().Set("Access-Control-Allow-Credentials", "true")

		next.ServeHTTP(w, r)
	})
}

package sessionstore

import (
	"lanacheckout/appapi/shared/appconfig"
	"net/http"

	"github.com/gorilla/sessions"
)

var (
	//AppCookieStore is the handler to get and set sessions/cookie items
	appCookieStore *sessions.CookieStore
)

//Init the session package getting settings from config.env
func Init() {
	appCookieStore = sessions.NewCookieStore([]byte(appconfig.EnvKeys.WebCookieKey))
}

//GetSessionValue retrive the session value for the passes sessionVarName
func GetSessionValue(r *http.Request, sessionVarName string) (string, bool) {
	session, _ := appCookieStore.Get(r, appconfig.EnvKeys.WebCookieName)
	sID, ok := session.Values[sessionVarName].(string)
	return sID, ok
}

//SetSessionValue set session var value
func SetSessionValue(w http.ResponseWriter, r *http.Request, sessionVarName, sessionVarValue string) string {
	session, _ := appCookieStore.Get(r, appconfig.EnvKeys.WebCookieName)
	session.Values[sessionVarName] = sessionVarValue
	session.Save(r, w)
	return sessionVarValue
}

//ClearSessionValue clean the session var
func ClearSessionValue(w http.ResponseWriter, r *http.Request, sessionVarName string) {
	session, _ := appCookieStore.Get(r, appconfig.EnvKeys.WebCookieName)
	session.Values[sessionVarName] = ""
	session.Options.MaxAge = -1
	session.Save(r, w)
}

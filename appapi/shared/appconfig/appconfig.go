package appconfig

import (
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"

	"github.com/joho/godotenv"
)

//Keys struct for config.evnv keys
type Keys struct {
	Env                    string
	WebPort                string
	WebUIPath              string
	WebAPIVersion          string
	WebCookieKey           string
	WebCookieName          string
	WebCookieSessionIDName string
}

var (
	//EnvKeys has the enviroment keys
	EnvKeys Keys
)

//Init initialize environment neutral vars
func Init() {
	configFile := "config.env"

	isWindowsOS := true
	log.Println("App running on:", runtime.GOOS)
	if runtime.GOOS != "windows" {
		isWindowsOS = false
	}
	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	if !isWindowsOS {
		configFile = filepath.Join(dir, configFile)
	}
	err := godotenv.Load(configFile)

	if err != nil {
		log.Println("CONFIG: config.env not loaded... using default settings")
		//default values in case that is not possible to load the config file
		EnvKeys.Env = "dev"
		EnvKeys.WebPort = ":8000"
		EnvKeys.WebUIPath = "/appweb/dist/appweb"
		EnvKeys.WebAPIVersion = "/api/v1"
		EnvKeys.WebCookieKey = "A4JBA9G2upLyeAx8"
		EnvKeys.WebCookieName = "lana-shop"
		EnvKeys.WebCookieSessionIDName = "session-id"
	} else {
		// initialize environment neutral vars
		EnvKeys.Env = os.Getenv("ENVIRONMENT")
		EnvKeys.WebPort = ":" + os.Getenv("WEB_PORT")
		EnvKeys.WebUIPath = os.Getenv("WEB_UI_PATH")
		EnvKeys.WebAPIVersion = os.Getenv("WEB_API_VERSION")
		EnvKeys.WebCookieKey = os.Getenv("WEB_COOKIE_KEY")
		EnvKeys.WebCookieName = os.Getenv("WEB_COOKIE_NAME")
		EnvKeys.WebCookieSessionIDName = os.Getenv("WEB_SESSION_ID_NAME")
	}

	if !isWindowsOS {
		EnvKeys.WebUIPath = filepath.Join(dir, EnvKeys.WebUIPath)
	} else {
		EnvKeys.WebUIPath = "." + EnvKeys.WebUIPath
	}

	if _, err := os.Stat(EnvKeys.WebUIPath); os.IsNotExist(err) {
		log.Println("ERROR: WebUIPath:", err.Error())
	}
}

//IsDebugMode dev or prod env
func (c *Keys) IsDebugMode() bool {
	return c.Env == "dev" || c.Env == "debug"
}

func toBoolOrDefault(v string, dv bool) bool {
	b, err := strconv.ParseBool(v)
	if err != nil {
		return dv
	}
	return b
}

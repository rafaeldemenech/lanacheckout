package model

//ProductData represents the product model to return data in the api
type ProductData struct {
	Code     string  `json:"code" db:"code"`
	Name     string  `json:"name" db:"name"`
	Price    float32 `json:"price" db:"price"`
	DealInfo string  `json:"deal_info" db:"deal_info"`
}

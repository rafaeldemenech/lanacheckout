package model

//ShoppingCartItemData represents the product in the basket model to return data in the api
type ShoppingCartItemData struct {
	Code     string  `json:"code" db:"code"`
	Name     string  `json:"name" db:"name"`
	Price    float32 `json:"price" db:"price"`
	Quantity int     `json:"quantity" db:"quantity"`
}

//ShoppingCartSummary represents the shopping cart totals and info data
type ShoppingCartSummary struct {
	TotalItems     int         `json:"total_items"`
	SubtotalValue  float32     `json:"subtotal_value"`
	TotalDiscounts float32     `json:"total_discounts"`
	TotalValue     float32     `json:"total_value"`
	DealsApplied   []DealModel `json:"deals_applied"`
}

//DealModel represents the deal item in the shopping cart summary
type DealModel struct {
	Title         string  `json:"title"`
	Description   string  `json:"description"`
	Quantity      int     `json:"quantity"`
	TotalValue    float32 `json:"total_value"`
	TotalDiscount float32 `json:"total_discount"`
}

package product

// New returns a new instance of the endpoint.
func New() *Endpoint {
	return &Endpoint{}
}

// Endpoint to use as type to call the controller func
type Endpoint struct {
}

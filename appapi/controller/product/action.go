package product

import (
	"lanacheckout/appapi/model"
	"lanacheckout/appapi/shared/response"
	"lanacheckout/appapi/shared/structcopy"
	"lanacheckout/appapi/store"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

//Get all products available and return a list of model.ProductData as data object
func (ctrl *Endpoint) Get(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {

		prod := store.NewProduct()
		prods := prod.Get()

		// Copy the items to the JSON model.
		arr := make([]model.ProductData, 0)

		for _, prodItem := range prods {

			item := new(model.ProductData)
			err := structcopy.ByTag(&prodItem, "db", item, "db")

			if err != nil {
				response.SendError(w, http.StatusInternalServerError, err.Error())
				return
			}

			arr = append(arr, *item)
		}

		response.Send(w, http.StatusOK, response.ItemFound, len(arr), arr)
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
}

//GetByCode retrieve a product by code and return a list of model.ProductData as data object with one object
func (ctrl *Endpoint) GetByCode(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {

		vars := mux.Vars(r)

		if searchCode := vars["code"]; len(searchCode) > 0 {

			prod := store.NewProduct()
			itemFound := prod.GetByCode(strings.ToUpper(searchCode))

			if itemFound {

				// Copy the items to the JSON model.
				arr := make([]model.ProductData, 0)

				item := new(model.ProductData)
				err := structcopy.ByTag(prod, "db", item, "db")

				if err != nil {
					response.SendError(w, http.StatusInternalServerError, err.Error())
					return
				}

				arr = append(arr, *item)

				response.Send(w, http.StatusOK, response.ItemFound, len(arr), arr)
			} else {
				response.Send(w, http.StatusOK, response.ItemNotFound, 0, nil)
			}
		} else {
			response.SendError(w, http.StatusBadRequest, "code param missing or with errors")
		}
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
	return
}

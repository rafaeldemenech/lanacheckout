package shoppingcart

import (
	"lanacheckout/appapi/store"
	"testing"
)

const (
	CodePen     = "PEN"
	PricePen    = 5.0
	CodeTShirt  = "TSHIRT"
	PriceTShirt = 20.0
	CodeMug     = "MUG"
	PriceMug    = 7.5
)

func TestSummaryBasketCalculation(t *testing.T) {
	// T1
	// Items: PEN, TSHIRT, MUG
	// Total: 32.50€
	//arrange
	cart := []store.ShoppingCartItem{
		store.ShoppingCartItem{
			Code:     CodePen,
			Price:    PricePen,
			Quantity: 1,
		},
		store.ShoppingCartItem{
			Code:     CodeTShirt,
			Price:    PriceTShirt,
			Quantity: 1,
		},
		store.ShoppingCartItem{
			Code:     CodeMug,
			Price:    PriceMug,
			Quantity: 1,
		},
	}
	//act
	result := summaryBasketCalcs(cart)
	//assert
	if result.TotalValue != 32.5 {
		t.Error("Failed to calc the basket total price for T1: PEN, TSHIRT, MUG")
	}

	// T2
	// Items: PEN, TSHIRT, PEN
	// Total: 25.00€
	//arrange
	cart = []store.ShoppingCartItem{
		store.ShoppingCartItem{
			Code:     CodePen,
			Price:    PricePen,
			Quantity: 2,
		},
		store.ShoppingCartItem{
			Code:     CodeTShirt,
			Price:    PriceTShirt,
			Quantity: 1,
		},
	}
	//act
	result = summaryBasketCalcs(cart)
	//assert
	if result.TotalValue != 25 {
		t.Error("Failed to calc the basket total price for T2: PEN, TSHIRT, PEN")
	}

	// T3
	// Items: TSHIRT, TSHIRT, TSHIRT, PEN, TSHIRT
	// Total: 65.00€
	//arrange
	cart = []store.ShoppingCartItem{
		store.ShoppingCartItem{
			Code:     CodePen,
			Price:    PricePen,
			Quantity: 1,
		},
		store.ShoppingCartItem{
			Code:     CodeTShirt,
			Price:    PriceTShirt,
			Quantity: 4,
		},
	}
	//act
	result = summaryBasketCalcs(cart)
	//assert
	if result.TotalValue != 65 {
		t.Error("Failed to calc the basket total price for T3: TSHIRT, TSHIRT, TSHIRT, PEN, TSHIRT")
	}

	// T4
	// Items: PEN, TSHIRT, PEN, PEN, MUG, TSHIRT, TSHIRT
	// Total: 62.50€
	//arrange
	cart = []store.ShoppingCartItem{
		store.ShoppingCartItem{
			Code:     CodePen,
			Price:    PricePen,
			Quantity: 3,
		},
		store.ShoppingCartItem{
			Code:     CodeTShirt,
			Price:    PriceTShirt,
			Quantity: 3,
		},
		store.ShoppingCartItem{
			Code:     CodeMug,
			Price:    PriceMug,
			Quantity: 1,
		},
	}
	//act
	result = summaryBasketCalcs(cart)
	//assert
	if result.TotalValue != 62.5 {
		t.Error("Failed to calc the basket total price for T3: PEN, TSHIRT, PEN, PEN, MUG, TSHIRT, TSHIRT")
	}

}

package shoppingcart

import (
	"lanacheckout/appapi/model"
	"lanacheckout/appapi/shared/appconfig"
	"lanacheckout/appapi/shared/response"
	"lanacheckout/appapi/shared/sessionstore"
	"lanacheckout/appapi/shared/structcopy"
	"lanacheckout/appapi/store"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
)

//Get shopping cart list of products by current session
func (ctrl *Endpoint) Get(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {

		sID, ok := sessionstore.GetSessionValue(r, appconfig.EnvKeys.WebCookieSessionIDName)

		if !ok || len(sID) == 0 {
			response.Send(w, http.StatusOK, response.ItemNotFound, 0, nil)
		} else {

			shoppingCartItem := store.NewShoppingCartItem()
			cart := shoppingCartItem.Get(sID)

			if cart == nil {
				sessionstore.ClearSessionValue(w, r, appconfig.EnvKeys.WebCookieSessionIDName)
				response.Send(w, http.StatusOK, response.ItemNotFound, 0, nil)
			} else {

				// Copy the items to the JSON model.
				arr := make([]model.ShoppingCartItemData, 0)

				for _, cartItem := range cart {

					item := new(model.ShoppingCartItemData)
					err := structcopy.ByTag(&cartItem, "db", item, "db")

					if err != nil {
						response.SendError(w, http.StatusInternalServerError, err.Error())
						return
					}

					arr = append(arr, *item)
				}

				response.Send(w, http.StatusOK, response.ItemFound, len(arr), arr)
			}
		}
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
}

//AddItem to shopping cart if first time will create session id to store the shopping list
func (ctrl *Endpoint) AddItem(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {

		vars := mux.Vars(r)

		if searchCode := vars["code"]; len(searchCode) > 0 {

			prod := store.NewProduct()
			itemFound := prod.GetByCode(strings.ToUpper(searchCode))

			qty, err := strconv.Atoi(vars["quantity"])

			if itemFound && qty > 0 && err == nil {

				sID, ok := sessionstore.GetSessionValue(r, appconfig.EnvKeys.WebCookieSessionIDName)

				if !ok || len(sID) == 0 {
					sUID := uuid.NewV4()
					sID = sessionstore.SetSessionValue(w, r, appconfig.EnvKeys.WebCookieSessionIDName, sUID.String())
				}

				shoppingCartItem := store.NewShoppingCartItem()
				shoppingCartItem.AddItem(sID, prod.Code, prod.Name, prod.DealInfo, qty, prod.Price)

				response.Send(w, http.StatusOK, response.ItemCreated, 1, nil)
			} else {
				response.Send(w, http.StatusOK, response.ItemNotFound, 0, err)
			}
		} else {
			response.SendError(w, http.StatusBadRequest, "code param missing or with errors")
		}
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
}

//RemoveItem to shopping cart from the session id shopping list if prod exists
func (ctrl *Endpoint) RemoveItem(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {

		vars := mux.Vars(r)

		if searchCode := vars["code"]; len(searchCode) > 0 {

			prod := store.NewProduct()
			itemFound := prod.GetByCode(strings.ToUpper(searchCode))
			qty, err := strconv.Atoi(vars["quantity"])

			sID, ok := sessionstore.GetSessionValue(r, appconfig.EnvKeys.WebCookieSessionIDName)

			if itemFound && ok && len(sID) > 0 {
				shoppingCartItem := store.NewShoppingCartItem()
				shoppingCartItem.RemoveItem(sID, prod.Code, qty)

				response.Send(w, http.StatusOK, response.ItemUpdated, 1, nil)
			} else {
				response.Send(w, http.StatusOK, response.ItemNotFound, 0, err)
			}
		} else {
			response.SendError(w, http.StatusBadRequest, "code param missing or with errors")
		}
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
}

//RemoveBasket remove/clean basket from session
func (ctrl *Endpoint) RemoveBasket(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		sID, ok := sessionstore.GetSessionValue(r, appconfig.EnvKeys.WebCookieSessionIDName)

		if ok && len(sID) > 0 {

			shoppingCartItem := store.NewShoppingCartItem()
			shoppingCartItem.RemoveCart(sID)

			response.Send(w, http.StatusOK, response.ItemDeleted, 1, nil)
		} else {
			response.Send(w, http.StatusOK, response.ItemNotFound, 0, nil)
		}
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
}

//ShowAllCarts retrieve all carts in the current execution, it will retrive all active sessions shopping cart
func (ctrl *Endpoint) ShowAllCarts(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {

		shoppingCartItem := store.NewShoppingCartItem()
		cart := shoppingCartItem.GetAllCarts()

		response.Send(w, http.StatusOK, response.ItemFound, len(cart), cart)
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
}

//CalcBasketPrice retrieve the total value for the shopping cart by the called session
func (ctrl *Endpoint) CalcBasketPrice(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		sID, ok := sessionstore.GetSessionValue(r, appconfig.EnvKeys.WebCookieSessionIDName)

		if !ok || len(sID) == 0 {
			response.Send(w, http.StatusOK, response.ItemNotFound, 0, nil)
		} else {

			shoppingCartItem := store.NewShoppingCartItem()
			cart := shoppingCartItem.Get(sID)

			if cart == nil {
				sessionstore.ClearSessionValue(w, r, appconfig.EnvKeys.WebCookieSessionIDName)
				response.Send(w, http.StatusOK, response.ItemNotFound, 0, nil)

			} else {

				respData := summaryBasketCalcs(cart)

				response.Send(w, http.StatusOK, response.ItemFound, respData.TotalItems, respData)
			}
		}
		return
	}
	response.SendError(w, http.StatusNotImplemented, "")
}

package shoppingcart

import (
	"lanacheckout/appapi/model"
	"lanacheckout/appapi/store"
)

//summaryBasketCalcs calculate the shopping cart summary (totals) applying some deal or promotion for specific items
func summaryBasketCalcs(cart []store.ShoppingCartItem) model.ShoppingCartSummary {
	returnData := new(model.ShoppingCartSummary)
	deals := make([]model.DealModel, 0)

	var totalValue float32
	var totalDiscount float32
	var totalItems int

	for _, item := range cart {
		switch item.Code {
		case "PEN":
			itemsToDiscount := item.Quantity / 2
			if itemsToDiscount > 0 {
				deal := new(model.DealModel)
				deal.Title = item.Name
				deal.Description = item.DealInfo
				deal.Quantity = itemsToDiscount
				deal.TotalDiscount = float32(itemsToDiscount) * item.Price

				deals = append(deals, *deal)

				totalDiscount += deal.TotalDiscount
				totalValue += deal.TotalValue
			}
		case "TSHIRT":
			if item.Quantity > 2 {
				deal := new(model.DealModel)
				deal.Title = item.Name
				deal.Description = item.DealInfo
				deal.Quantity = 1
				deal.TotalDiscount = float32(item.Quantity) * item.Price * 0.25
				deals = append(deals, *deal)

				totalDiscount += deal.TotalDiscount
				totalValue += deal.TotalValue
			}
		}

		totalValue += float32(item.Quantity) * item.Price
		totalItems += item.Quantity

	}

	returnData.DealsApplied = deals
	returnData.TotalItems = totalItems
	returnData.SubtotalValue = totalValue
	returnData.TotalDiscounts = totalDiscount
	returnData.TotalValue = totalValue - totalDiscount

	return *returnData
}

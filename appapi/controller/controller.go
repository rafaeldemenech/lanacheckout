package controller

import (
	"lanacheckout/appapi/controller/product"
	"lanacheckout/appapi/controller/shoppingcart"
)

//Default response messages
const (
	ItemCreated      = "item created"
	ItemExists       = "item already exists"
	ItemNotFound     = "item not found"
	ItemFound        = "item found"
	ItemsFound       = "items found"
	ItemsFindEmpty   = "no items to find"
	ItemUpdated      = "item updated"
	ItemDeleted      = "item deleted"
	ItemsDeleted     = "items deleted"
	ItemsDeleteEmpty = "no items to delete"

	FriendlyError = "an error occurred, please try again later"
)

//Controllers
var (
	ProductCtrl      product.Endpoint
	ShoppingCartCtrl shoppingcart.Endpoint
)

package store

//ShoppingCartItem represent the repository model for Lana Shopping Cart
type ShoppingCartItem struct {
	Code     string  `db:"code"`
	Name     string  `db:"name"`
	Price    float32 `db:"price"`
	DealInfo string  `db:"deal_info"`
	Quantity int     `db:"quantity"`
}

var (
	shoppingCarts map[string][]ShoppingCartItem
)

//NewShoppingCartItem returns a new store object
func NewShoppingCartItem() *ShoppingCartItem {
	return &ShoppingCartItem{}
}

//GetAllCarts stored
func (x *ShoppingCartItem) GetAllCarts() map[string][]ShoppingCartItem {
	return shoppingCarts
}

//Get the shopping cart by session
func (x *ShoppingCartItem) Get(sessionID string) []ShoppingCartItem {
	return shoppingCarts[sessionID]
}

//AddItem into the shopping cart by session
func (x *ShoppingCartItem) AddItem(sessionID, prodCode, name, dealInfo string, qty int, price float32) {
	var item ShoppingCartItem
	item.Code = prodCode
	item.Quantity = qty
	item.Name = name
	item.Price = price
	item.DealInfo = dealInfo

	cart := shoppingCarts[sessionID]

	if cart == nil {
		cart = []ShoppingCartItem{}
		cart = append(cart, item)
	} else {

		productFound := false
		for i, cItem := range cart {
			if cItem.Code == item.Code {
				cart[i].Quantity += item.Quantity
				productFound = true
				break
			}
		}
		if !productFound {
			cart = append(cart, item)
		}
	}
	shoppingCarts[sessionID] = cart
}

//RemoveItem from the shopping cart by session
func (x *ShoppingCartItem) RemoveItem(sessionID, prodCode string, qty int) {
	cart := shoppingCarts[sessionID]

	if cart != nil {
		idx := -1

		for i, cItem := range cart {
			if cItem.Code == prodCode {
				if qty == 0 || qty >= cart[i].Quantity {
					idx = i
				} else {
					cart[i].Quantity -= qty
					shoppingCarts[sessionID] = cart
				}
				break
			}
		}

		if idx > -1 {
			shoppingCarts[sessionID] = removeItemByIndex(cart, idx)
			if len(shoppingCarts[sessionID]) == 0 {
				delete(shoppingCarts, sessionID)
			}
		}
	}
}

//RemoveCart delete map item that represents the shopping cart
func (x *ShoppingCartItem) RemoveCart(sessionID string) {
	cart := shoppingCarts[sessionID]

	if cart != nil {
		delete(shoppingCarts, sessionID)
	}
}

//removeItemByIndex split slice removing idx item
func removeItemByIndex(s []ShoppingCartItem, index int) []ShoppingCartItem {
	return append(s[:index], s[index+1:]...)
}

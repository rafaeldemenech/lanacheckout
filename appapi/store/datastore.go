package store

//Init the data store
func Init() {

	prod1 := Product{
		Code:     "PEN",
		Name:     "Lana Pen",
		Price:    5,
		DealInfo: "Buy 2 get 1 free.",
	}

	prod2 := Product{
		Code:     "TSHIRT",
		Name:     "Lana T-Shirt",
		Price:    20,
		DealInfo: "Buy 3 or more, the price per unit is reduced by 25%.",
	}

	prod3 := Product{
		Code:     "MUG",
		Name:     "Lana Coffee Mug",
		Price:    7.5,
		DealInfo: "",
	}

	products = append(products, prod1, prod2, prod3)
	shoppingCarts = make(map[string][]ShoppingCartItem)

}

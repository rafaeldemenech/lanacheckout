package store

//Product represent the repository model for Lana Products
type Product struct {
	Code     string  `db:"code"`
	Name     string  `db:"name"`
	Price    float32 `db:"price"`
	DealInfo string  `db:"deal_info"`
}

var (
	products []Product
)

//NewProduct returns a new store object
func NewProduct() *Product {
	return &Product{}
}

//Get all products
func (x *Product) Get() []Product {
	return products
}

//GetByCode product by code passed as param and load to itself
func (x *Product) GetByCode(code string) bool {

	productFound := false

	for _, prod := range products {
		if prod.Code == code {
			x.Code = prod.Code
			x.Name = prod.Name
			x.Price = prod.Price
			x.DealInfo = prod.DealInfo

			productFound = true
			break
		}
	}
	return productFound
}
